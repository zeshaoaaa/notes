## 前言

BDD（行为驱动开发，Behavior-Driven Developemnt）是基于 TDD 衍生而来的一种开发方式，如果你还不了解 TDD 的话，可以先看我另一篇讲 TDD 的文章。

### 1. 普通的产品文档存在哪些问题？

我们在开发产品的过程中，或多或少都存在产品文档描述不清晰，导致的开发人员误解了产品经理的需求的现象，比如下面这个文档。

红色方框表明的是快捷消息的入口，但是文档上只说了入口，并没有说选择了快捷信息后，要怎么操作。

结果导致 iOS 的操作是选择了快捷信息后就发送消息，Android 的操作是选择了快捷消息后，把快捷消息填入到输入框中。



![](https://user-gold-cdn.xitu.io/2020/5/26/1725037c5ab4d1e2?w=676&h=923&f=png&s=128251)



还有一种情况就是像下面这样的描述，如果开发人员或测试人员不小心少看了一行，那可能就有一个业务规则被忽略，导致要重新做。

重新做也就是返工，一次能做好的事情要分成两次，造成最大的浪费就是测试人员工作的耽误。

如果测试人员反馈了 Bug ，而开发人员手头上有其他工作的话，测试人员就只能等着开发人员解决缺陷，甚至有些用例就卡在一个地方执行不下去了。

![](https://user-gold-cdn.xitu.io/2020/5/26/172503b21808bb1f?w=1040&h=774&f=png&s=116982)



可能有的人会觉得这都是产品经理的错、都是开发人员、测试人员的错，找出那个粗心的人，叫他注意点，然后下次就不会出错了。

假如现在别人说你出了问题，让你下次注意点，谨慎点。你除了说好的，还能做什么？你能保证你下次一定不会忽略任何东西吗？

那还有没有其他办法呢？别急，继续往下看。

### 2. 内容概览

![内容概览](https://user-gold-cdn.xitu.io/2020/6/23/172dfc0ebd68dbe6?w=760&h=459&f=png&s=48001)



文中的代码在文章的最下方会有 GitHub 链接。

## 1. BDD 入门

BDD 有下面几个特点。

* 聚焦

  BDD 可以帮助研发团队把注意力聚焦在识别、理解和构建对业务有价值的特性，并且确保这些特性的设计和实现良好。

* 示例

  实践 BDD ，意味着研发团队用描述系统行为的具体示例进行沟通，以便让大家都能理解特性如何向业务提供价值的。

* 协作

  BDD 鼓励产品经理、开发人员和测试人员更紧密地协作，通过让大家以一种易于测试、易于开发、无歧义的方式表达需求。

* 测试

  用 BDD 描述的需求可以转化为自动化测试，这些自动化测试可以帮助开发人员自测，还是一份实时更新的功能描述文档。



### 1.1 BDD 由来

![](https://user-gold-cdn.xitu.io/2020/5/26/1724ea59a063d11e?w=1280&h=720&f=jpeg&s=82925)

BDD 是由 Dan North 于 2000 年提出的，在这之前，他一直在教授 TDD 开发方法。

North 在当时发现 TDD 有时会让开发者陷入一种过于关注细节的情况，让开发者在无意中忽略了系统应满足的业务需求。

比如用 TDD 开发一个银行 App ，这个 App 需要转账和存款的功能，那么我们可以建立对应的测试。

![](https://user-gold-cdn.xitu.io/2020/2/1/1700100d82e90ac1?w=860&h=828&f=png&s=95420)

这样的测试比没有测试要强，但这样的测试存在的问题就是预期输出不明确，有可能出现测试代码与生产代码强关联，而与业务需求无关的情况，这样如果想重构生产代码的实现时，也要重构测试代码。

而 North 在教授 TDD 时发现，以句子命名的单元测试，可以帮助开发者写出意义明确的测试，意义明确的测试，能让开发者更高效地写出高质量的测试和生产代码，比如下面这样的。

![](https://user-gold-cdn.xitu.io/2020/2/1/17000ffae9b44cf6?w=1084&h=828&f=png&s=118256)

以这种方式命名的测试，和单元测试比起来与业务需求的描述更接近。

这样的测试专注于系统应用的行为，这些测试的目的，则是表达与验证这些行为，而且这种意义明确的方式编写的测试，维护起来也更方便。

由于这些优点，North 就用一种新的名字命名这种开发方式：行为驱动开发（BDD）。



### 1.2 BDD 开发流程

一般开发流程是下面这样的，用户告诉产品经理自己想要的功能，然后产品经理编写文档，开发人员和测试人员则把需求文档转化为代码和测试用例。

使用这种流程进行开发，经常会出现开发人员或测试人员误解需求的情况，结果就是项目不断返工，不断重新开发、重新测试，导致项目延期。

![一般开发流程](https://user-gold-cdn.xitu.io/2020/2/1/17000172fe7df034?w=2540&h=2060&f=png&s=628993)



而 BDD 则是用下图这种方式，大家通过围绕着具体实例进行沟通，以一种能轻易转化为自动化测试的语言，描述需求的具体实例，从而减少一般的开发流程，在需求理解、转换过程中出现的信息丢失和误解的情况。

![BDD 开发流程](https://user-gold-cdn.xitu.io/2020/2/1/170004fee0403c4a?w=3176&h=2238&f=png&s=913312)



### 1.3. BDD 过程



![](https://user-gold-cdn.xitu.io/2020/2/1/17001295167279e4?w=2142&h=2240&f=png&s=521500)



上图包含了 BDD 过程中的各个活动，BDD 的第一步是识别产品要实现的业务目标，然后是找出能满足该业务目标的功能。

BDD 需要与用户合作，使用具体实例描述功能，实例是以自动化的可执行说明（Executable Specifications）描述的。

实例和可执行说明都可以验证软件的期望行为，并提供自动更新的技术文档和功能文档。

在代码层面，转化为低阶说明（Low-Level Specification）的可执行说明，能帮助开发者写出高质量、易于测试、文档健全、易于使用且易于维护的代码。

每一个特性的实现，都会经历上面这些活动。



##### 1. 具体实例

实践 BDD 的第一步是明确业务目标，比如一个银行 App 的业务目标可能是：通过简单且方便的账户管理功能吸引更多客户。

实践 BDD 时，当需要实现一个功能，团队成员需要与用户一起定义功能的各个使用场景，从而明确用户的具体需要。

用户则需要帮助建立起具体实例（Concrete Examples），这些实例则是功能在各个场景下应有的输出，比如下面这样的。

下面的特性文件是用 Gherkin 语言写的，关于 Gherkin 的用法后面会有更多介绍。

![](https://user-gold-cdn.xitu.io/2020/2/2/1700561217e30f52?w=906&h=728&f=png&s=116498)

一般的需求文档则充斥着歧义、对读者知识背景的假设，从而导致读者，如开发人员和测试人员对需求的误解。

而 BDD 中的实例能清晰、准确且无歧义的需求说明，是一种高效沟通方式，因此具体实例在 BDD 中扮演者非常重要的角色。

实例还是一种探索和扩展知识的方式，当用户提出一个功能的具体实例时，项目成员可以提出边界示例，避免在开发时才发现没有讨论到边界值。

这些边界情况往往会是测试人员提出的，通过团队成员一起讨论讨功能可能遇到的多个场景，就能减少测试人员在产品开发完毕后，才提出并把边界问题抛给产品人员，导致项目返工、延期。



##### 2. 可执行说明

可执行说明（Executable Specifications）是一些自动化测试，这些测试能描述应用的业务需求，并验证应用是否满足了业务需求。

可执行说明将由 Cucumber 执行，在后面会有关于 Cucumber 的更多介绍。

低阶说明（Low-level Specifications）是一些单元测试，这些测试能描述应用内部函数的行为，并验证这些函数是否符合预期的行为，是开发人员可以参考的一份良好的技术文档。

低阶说明可以用 Spek 编写，关于 Spek 的使用后面会讲到，可执行说明不只是给开发人员看的技术报告，而且还是一份可以让团队所有人都能看懂的产品说明文档。

这份文档与普通的需求文档不同，这份文档必须要随着需求的变化实时更新，否则测试就会失败，而普通的需求文档往往是编写完毕后，哪怕需求有变化，也不会有及时的更新，尤其是已发布功能的需求变动。

可执行说明生成的测试报告对不同人员的意义如下。

* 开发人员

  了解已有功能的工作情况；

* 测试人员

  了解哪些功能已经实现了，可以开始验证了；

* 产品经理/项目经理

  了解项目的真实前进展，并决定哪些功能可以发布到生产环境；

* 用户/业务人员

  了解如何使用这个产品；



这份文档将随着需求的变化实时更新，且易于维护，包含了示例以及对应的代码背后的期望行为。

在本文的第二部分 BDD 示例，将会进一步展示可执行说明和低阶说明的具体形式，新加入的开发人员，往往需要一定的时间才能了解现有的代码，而使用低阶说明，则可以缩短这个时间，让新加入的人员快速了解系统的运作方式。





### 1.4 Cucumber

Cucumber 是一个验收测试工具、一个协作工具，最初只有 Ruby 版的，随着时间的推移，Cucumber 已经用很多种不同的编程语言实现了。

Cucumber 测试直接与开发人员的代码关联，同时还是用一种以产品人员、业务专家能够理解中间语言编写的.通过一起编写测试，团队成员不仅能确定下一步要实现的功能，也让大家对于要开发的软件功能有一致的概念。

Cucumber 测试和一般的需求文档一样，能被相关人员阅读和修改，和普通需求文档不同的是，Cucumber 编写的验收测试能通过计算机进行验证。

而且使用 Cucumber 编写的验收测试，必须要与需求的变化对应上，要是最新的，否则测试就会失败，这样就避免了一般的需求文档更新不及时的问题。



#### 1.4.1 Gherkin 

Gherkin 是一门用于编写 Cucumber 验收测试的语言，用 Gherkin 写的特性文件的扩展名为 .feature 。

特性文件包含了功能的简短描述、该功能下的多个场景，以及场景下的多个例子。

Gherin 支持四十多种语言，如果想使用中文编写特性文件，只需要在特性文件的头部添加 #language: zh-CN。

##### 1. 中英文对照表

Gherkin 支持的关键字中英文对照表如下：

| 英文关键字       | 中文关键字                   |
| ---------------- | ---------------------------- |
| feature          | "功能"                       |
| background       | "背景"                       |
| scenario         | "场景", "剧本"               |
| scenario_outline | "场景大纲", "剧本大纲"       |
| examples         | "例子"                       |
| given            | "* ", "假如", "假设", "假定" |
| when             | "* ", "当"                   |
| then             | "* ", "那么"                 |
| and              | "* ", "而且", "并且", "同时" |
| but              | "* ", "但是"                 |
| given (code)     | "假如", "假设", "假定"       |
| when (code)      | "当"                         |
| then (code)      | "那么"                       |
| and (code)       | "而且", "并且", "同时"       |
| but (code)       | "但是"                       |



##### 2. 功能

每一个 Gherkin 特性文件都是从 Feature（功能）关键字开始的，Feature 下面可以添加这几个关键字：

* Scenario（场景）
* Background（背景）
* Scenario Outline（场景大纲）

我们可以给 Feature 添加描述，一般是以用户故事的形式来编写的。

用户故事的格式为：

作为 ...（利益相关人）

我想 ...（做什么事情）

以便 ...（达到什么目的）

![](https://user-gold-cdn.xitu.io/2020/2/3/1700a4275cfa06e9?w=1004&h=894&f=png&s=145334)





##### 2. 场景

场景大纲和场景的区别就在于场景大纲是可以添加例子（Examples）的，例子中可以放置一些我们想要输入给程序的数据。

如果有多个例子，Cucumber 就会运行多次，比如有两个例子的话，Cucumber 就会用两个例子中的不同数据来执行测试。

为了描述我们期望的软件行为，每个功能下都要包含一些场景。

每个场景是特定情况下，系统应有行为的具体实例，这些场景汇总起来，就是期望的功能行为。

通常一个特性会有 5 到 20 个场景，我们可以用多个场景探索边界值、正常和异常执行路径。

定义场景的模式通常是下面这样的：

1. 状态准备（Given，比如跳转到登录页）
2. 触发变化（When，比如点击登录按钮）
3. 验证输出（Then，比如提示“登录成功”）

在 Gherkin 中，Given、When、Then 用于确定场景中三个不同的部分。

Given 用于建立场景发生的上下文，When 表示用户以某种方式与系统交互，Then 则用于检查交互的结果是否符合预期。

场景中的每一行称为一个步骤（Step），我们可以用 And 和 But 为 Given、When、Then 加入更多步骤。

Cucumber 实际上并不关心每个步骤的是什么关键字，这些关键字大的作用主要是方便我们阅读。



##### 3. 例子

在前面提到的场景大纲下中有例子（Example），例子中放的是步骤定义代码中将读取到的数据。

例子可以定义多个，比如下面这样的。

![](https://user-gold-cdn.xitu.io/2020/2/4/1700f881d181d3be?w=1146&h=894&f=png&s=164652)





##### 4. 独立

在编写场景时，要注意场景必须是能独立执行的，不依赖于其他场景。

比如账户信息页下的场景，与登录页的场景要是无关的，不能说用上一个登录页场景登录的结果来验证账户信息页的功能。

否则在维护上会变得困难，比如登录页场景的例子变了之后，还要修改账户信息页的场景，可读性也会变差，比如想理解账户信息页的场景，还要先理解登录页的场景。



#### 1.4.2  Cucumber 层次结构

具体实例能帮助团队成员在系统构建前，把系统形象化，从而提升沟通的有效性。

团队成员在阅读这样的测试时，能与自己的理解进行对比，而且这样的测试能激发更进一步的思想，设想出更多的场景。

用这种风格编写的验收测试不仅是测试描述，而且还是可自动化执行的需求描述。

我们用 Cucumber 运行测试时，Cucumber 会从用普通语言编写的特性文件中，读取需求说明，并解析需要测试的场景（Scenario），然后运行这些场景，以达到测试的目的。

每个场景都是由一系列的步骤（Step）组成的，Cucumber 会一步步执行这些步骤，为了让 Cucumber 理解特性文件，特性文件按必须遵循 Gherkin 语法规则。

除了特性文件，我们还要给 Cucumber 提供一组步骤定义（Step Definition），步骤定义的代码将写在 Android 测试类中，对应的是特性文件中的每个步骤。

在一个成熟的测试集中，步骤定义本身可能只包含一两行测试代码，具体的工作都交给了支持代码（Support Code）完成。

领域特定的支持代码库知道如何执行软件的常见任务，一般会用一个自动化库（Automation Library），比如Espresso，用于与待测试系统进行交互。



Cucumber 从特性到自动化库的层次结构如下图所示：

![Cucumber层次结构](https://user-gold-cdn.xitu.io/2020/1/29/16ff0babaaec4fc6?w=1550&h=2110&f=png&s=201317)







## 2. BDD 示例

下面我们通过一个简单的示例，来看一个完整的 BDD 流程。

### 2.1 创建特性文件

首先我们看下怎么创建 Cucumber 特性文件。



##### 1. 添加 Gherkin 插件

第一步是添加 Gherkin 插件，这样 AS 就能对使用 Gherkin 写的特性文件中的代码进行语法高亮。



![](https://user-gold-cdn.xitu.io/2020/1/29/16ff122b59fbaf98?w=1060&h=709&f=png&s=216700)



##### 2. 添加文件模板

右键项目目录中的任意一个文件，选中 New 标签，可以看到一个 Edit File Templates 的选项。

![](https://user-gold-cdn.xitu.io/2020/1/29/16ff104ad8bb7d74?w=2232&h=1642&f=png&s=773365)



点击 Edit File Templates 后，点击加号，并把文件模板的名称改为 Cucumber feature file ，并把 Extension 后缀改为 feature 。

添加后模板后，可以看到这个文件模板的图标是一个绿色的，这就是 Gherkin 的 Logo。

![](https://user-gold-cdn.xitu.io/2020/1/29/16ff1248d793acfb?w=1514&h=1648&f=png&s=455129)



点击 OK 保存模板，再次点击 New 选项，可以看到现在多了一个 Cucumber feature file 模板。

![](https://user-gold-cdn.xitu.io/2020/1/29/16ff1285bae81145?w=1806&h=1268&f=png&s=485378)







##### 3. 添加 Cucumber 依赖

![](https://user-gold-cdn.xitu.io/2020/1/30/16ff5f30c5649cab?w=1306&h=862&f=png&s=155651)



##### 4. 创建测试运行器

接下来在 androidTest 下新建一个 test 目录，然后在这个目录下创建测试运行器 CucumberTestRunner。

![](https://user-gold-cdn.xitu.io/2020/5/25/1724a734b67bbc9c?w=317&h=361&f=png&s=19869)

![](https://user-gold-cdn.xitu.io/2020/5/26/1724e8f873c82d41?w=1354&h=1512&f=png&s=277796)



CucumberTestRunner 的 CucumberOptions 注解用于填写运行 Cucumber 测试的相关选项，Cucumber插件会根据该注解中填写的选项进行初始化。

CucumberOptions 注解中有很多选项，最常用的是 features、glue 、tags 和 strict。

features 选项要填写特性文件的目录，glue 选项中要填写 Cucumber 步骤定义代码的目录。

strict 选项用于表明如果有未定义的步骤，那在测试运行前就会报出初始化错误，这样我们就不用等到测试都快结束了才发现测试跑不下去。

然后把默认的 InstrumentationRunner 改为 "...CucumberTestRunner"。



![](https://user-gold-cdn.xitu.io/2020/5/25/1724b3848286c169?w=1166&h=540&f=png&s=45196)



##### 5. 添加特性文件

接下来在 androidTest 目录下新建一个 assets/features 目录，这个目录用于存放前面提到的的登录功能的验收测试的特性文件。

这个目录不一定要叫 features ，如果你想使用其他名字的话，只要把 CucumberOptions 中的 features 选项的值改为对应的包名即可。



![](https://user-gold-cdn.xitu.io/2020/5/25/1724a7b3df6b18fe?w=876&h=594&f=png&s=130967)



##### 6. 添加步骤定义

这是一个简单的登录页。

![](https://user-gold-cdn.xitu.io/2020/1/30/16ff51a7cbb1d445?w=800&h=1406&f=png&s=47519)





首先我们定义一个 EspressoExt ，用于简化步骤定义代码。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffca4db49cc137?w=1234&h=1428&f=png&s=247344)

然后编写新建一个 LoginSteps，编写 login.feature 中各个步骤对应的步骤定义代码。

![](https://user-gold-cdn.xitu.io/2020/2/4/1700f3e8b0cc2b99?w=1206&h=1428&f=png&s=237747)

验收测试先行，现在验收测试写完了，接下来我们来看下生产代码。





### 2.2  编写生产代码



##### 1. Koin 简介

Koin 是一个用 Kotlin 实现的依赖注入框架，你可以把它看成是傻瓜版的 Kotlin 实现的 Dagger。

之所以说是傻瓜版，是因为相比之下 Dagger 实在是太麻烦了。

之所以要用依赖注入框架，是因为我们现在要验证的是 App 里的逻辑，而不是完整真实的登录功能，我们不需要管后台返回什么。

我们要脱离真实的接口去运行测试，也就是我们要弄一个假的数据层 Model 。

如果我们的测试跟真实的接口关联在一起，而刚好测试服务器在部署或数据重置了。

那测试就一定会失败，这样建立好的测试就没意义了，经常要去改，非常浪费时间。

Koin 用起来很方便，而且也能达到解耦的目的，下面我们来看下 Koin 怎么用。



##### 2. 添加依赖

首先在需要 Koin 的模块的 build.gradle 中添加 Koin 依赖。

![](https://user-gold-cdn.xitu.io/2020/2/3/17009f41416c55ae?w=1004&h=528&f=png&s=70289)

##### 3. 初始化 Koin

然后在 Application 中调用 startKoin 方法，并把我们的登录模块放到 modules() 方法中。

下面的 get() 表示自动根据参数类型，获取在 startKoin 中声明好的对应的实例，在这里也就是获取 LoginModel 实例。

![](https://user-gold-cdn.xitu.io/2020/1/31/16ffa6c2cdab4767?w=1222&h=1462&f=png&s=202684)

##### 4. View

Activity 的实现很简单，只是设置了登录按钮的点击事件。

比较特别的是，presenter 是通过 by inject 声明的，表明 presenter 实例是由 Koin 容器注入的。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffc65064506f45?w=1274&h=1228&f=png&s=194753)

##### 5. Presenter

怎么通过 TDD 实现 LoginPresenter 在我的另一篇讲 TDD 的文章里讲过了，这里就不多说了，直接看代码。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffc67756888dd6?w=1066&h=1362&f=png&s=201720)

##### 6. Model

这里的请求是假的，也就是 Presenter 的请求逻辑是走不通的，只能通过替换依赖来模拟响应。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffc699fd33432b?w=1102&h=862&f=png&s=125319)



### 2.3 运行测试

##### 1. 运行测试任务

点击 AS 右侧的 Gradle 标签查看 Gradle 提供的任务，我们选择运行 connectedCheck ，也就是运行 UI 测试。

![](https://user-gold-cdn.xitu.io/2020/1/30/16ff5cbd651a31eb?w=1140&h=1734&f=png&s=173137)



##### 2. 查看测试报告

运行任务后，运行结果是测试失败，点击 AS 下方的 Run 标签，可以看到有一个测试报告的链接。

![](https://user-gold-cdn.xitu.io/2020/1/30/16ff5ecf4692074d?w=2170&h=1484&f=png&s=326098)



点击该链接，右键 index.html 上方文件名标签，选择用浏览器打开。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffc77eb1f4fdb5?w=1772&h=1710&f=png&s=571713)



打开后在浏览器中可以看到测试结果，这里可以看到有 3 个测试，对应的就是我们前面定义好的两个场景，第二个场景中的两个例子通过了。

而第一个场景是没有通过的，因为我们 Model 的登录请求根本就没实现。

这里报了一个 NoMatchingRootException 异常，也就是没有匹配到"登录成功"的 Toast 提示。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffc6ba3863b698?w=1968&h=1290&f=png&s=323441)



### 2.4 模拟登录请求

##### 1. 钩子方法

前面我们在 LoginSteps 中添加了登录特性的步骤定义代码，现在我们在这个类中添加模拟请求代码。

前面的测试由于 Model 中的请求登录方法没有实现所以失败了，这时候我们要弄一个假的实现，Koin 就派上用场了。

加载模拟对象这个步骤与业务需求无关，不适合写在特性文件中，所以我们使用钩子方法来加载模拟对象。



这里我们通过 Cucumber 提供的 @Before 和 @After 注解，这两个方法是 Cucumber 提供的钩子方法。

通过这两个方法，Cucumber 会执行我们想在测试前或测试后执行的操作。

在 setUp 中还可以接收到一个 Scenario 参数，我们可以用这个参数在不同的场景执行不同的初始化操作。

在这里的初始化操作中，调用了 Koin 提供的 loadKoinModules() 方法。

并通过 module 方法重新声明一个需要用于替换真实实现的 module ，这里的 factory() 的调用和 Application 中的不一样。

在这里，factory 中多了一个 override = true 参数，表明这是要替换原有实现的。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffc926a99c0829?w=1100&h=2496&f=png&s=383125)

##### 2. 再次运行测试

前面用 MockK 提供的 every() 方法模拟了 LoginModel 的 requestLogin() 方法，制造了一个假的响应，这样子 LoginPresenter 就能接收到回调，并显示对应的提示信息。

![](https://user-gold-cdn.xitu.io/2020/2/1/16ffc982cf88620f?w=1628&h=1130&f=png&s=185968)





### 2.5 Spek

Spek 是 JetBrains 团队开发的一门单元测试框架，可用于编写我们前面提到的低阶可执行说明（Low-Level Executable Specification）。

普通 JUnit 单元测试存在一个问题，就是测试代码往往与生产代码的当前实现强耦合了，也就是生产代码发生变化时，往往也需要修改测试代码，这样测试代码的维护成本就变高了。

而我们可以用 Spek 解决这个问题，Spek 编写的测试代码类似于 Gherkin 语法的形式，能更清晰的描述单元测试的意图。下面我们来看下怎么使用 Spek。

##### 1. 添加插件

在设置的 Plugins 中搜索 Spek 就可以看到 Spek Framework 插件。

有了这个插件，AS 才会识别用 Spek 写的测试，这样我们才可以像运行普通的 JUnit 测试一样运行 Spek 测试。

![](https://user-gold-cdn.xitu.io/2020/5/26/1724e92bd046b0fc?w=1006&h=722&f=png&s=78352)



##### 2. 添加依赖

这里看看就好了，具体的代码到文章底部的 GitHub 仓库中复制。

![](https://user-gold-cdn.xitu.io/2020/2/2/17005bb4e7fd304a?w=1340&h=1428&f=png&s=242966)

在 app 模块的 build.gradle 中添加 apply from: '../spek.gradle' ，然后在项目根目录的 build.gradle 中添加 JUnit 5 的插件依赖。

![](https://user-gold-cdn.xitu.io/2020/2/2/17005be40d1b9f51?w=1446&h=828&f=png&s=132889)

##### 3. JUnit 测试

如果我们现在需要测试一个用于验证手机号合法性的方法 isPhoneValid() ，那么用 JUnit 写这个测试的话是这样的。

![](https://user-gold-cdn.xitu.io/2020/2/2/17005dabfc2de830?w=1146&h=928&f=png&s=147489)

##### 4. Spek 测试

如果我们用 Spek 写的话，这个测试是下面这样的。

![](https://user-gold-cdn.xitu.io/2020/2/2/17005e3b3ab5ee83?w=984&h=1728&f=png&s=220109)

这样写虽然代码变多了，但是测试的可读性也变强了。

​	



### 2.6 测试覆盖率

测试覆盖率就是测试执行的生产代码的比率，比如 LoginPresenter 中有请求登录和请求验证码的方法，而测试只调用了请求登录方法，那测试覆盖率就是 50%。

下面我们来看下怎么用测试覆盖率统计工具 Jacoco 。

##### 1. 添加插件依赖

我们首先在项目根目录下的 build.gradle 中添加 Jacoco 插件的依赖。

![](https://user-gold-cdn.xitu.io/2020/5/26/1724eb13debbd6f1?w=974&h=1044&f=png&s=81548)



##### 2. 新建配置文件

然后创建 jacoco.gradle ，把 Jacoco 相关的配置放在里面，这个配置比一般的 Jacoco 配置要复杂，因为是多 productFlavor 和多模块统计的配置。

如果没有多模块的话，可以把 secondModule 相关的配置给删除。

![](https://user-gold-cdn.xitu.io/2020/5/26/1724ec88de7c778f?w=1886&h=4536&f=png&s=887305)



##### 3. 查看覆盖率报告

由于这个项目只有组合构建变量，只有两个构建类型 debug 和 release ，所以在这里看到的是 getDebugCoverage 和 getReleaseCoverage 任务。

运行这个任务后，这个任务是依赖了 UI 测试任务和单元测试任务，所以执行 getXXXCoverage 就是在执行测试后打开。

![](https://user-gold-cdn.xitu.io/2020/5/26/1724ed0b4acdceba?w=552&h=758&f=png&s=64963)



运行这个任务后，可以在 build/reports 下看到 jacoco 生成的覆盖率报告。

![](https://user-gold-cdn.xitu.io/2020/5/26/1724ede040cc23d3?w=423&h=751&f=png&s=54420)



由于 getXXXCoverage 调用了 open xxx/index.html ，所以会自动打开这个页面，下面就是统计结果，包含了按指令和按分支来算的测试覆盖率。

![](https://user-gold-cdn.xitu.io/2020/5/26/1724ee0bb2c89302?w=1051&h=318&f=png&s=74843)

点 org.jay.example.data 包，再点 BaseResponse 后，可以看到这个类的测试结果。

绿色的就是执行测试过程中被执行的代码，红色的就是没执行的。

![](https://user-gold-cdn.xitu.io/2020/5/26/1724ee6542d1c94a?w=693&h=557&f=png&s=78193)



##### 4. 现状

截止至 2020年05月，我们金千枝 Android 项目的测试覆盖率为 29%。

![](https://user-gold-cdn.xitu.io/2020/5/29/1725e3c6afff62a0?w=889&h=571&f=png&s=177860)



如果新开发的模块是按 BDD 的流程开发的，那按道理 Android 端新开发的功能应该没有问题了。

但实际上现在的特性文件是由我自己写的，我自己判断它对不对，如果我误解了需求或遗漏了某个边界场景，那建立的对应的测试的质量就不高，也就可能会出现其他问题。

如果后续能够加入特性文件评审的环节，让产品经理和测试人员提出他们对特性文件的看法，让大家对需求的理解都保持一致时，这样建立出来的白盒测试才更有价值，才能进一步减少缺陷和返工。





## 资料

##### 1. 参考资料

* [《The Cucumber for Java Book》](https://book.douban.com/subject/26345784/)
* [《BDD in Action》](https://book.douban.com/subject/24849622/)
* [《The RSpec Book》](https://book.douban.com/subject/3603693/)
* [cucumber系列（一） 如何让cucumber识别中文](https://www.cnblogs.com/mover/p/3671114.html)
* [Android BDD with Cucumber and Espresso — the full guide](https://medium.com/gumtree-dev-team/android-bdd-with-cucumber-and-espresso-the-full-guide-9c20cfcb8535)
* [CucumberOptions](https://www.toolsqa.com/cucumber/cucumber-options/)
* [Kotlin Spek](https://medium.com/@budioktaviyans/kotlin-spek-85adb749850e)
* [Set Android BDD Style with Kotlin Spek 2](https://medium.com/better-programming/set-android-bdd-style-with-kotlin-spek-2-14176f235d17)
* [Unified Code Coverage for Android: Revisited](https://proandroiddev.com/unified-code-coverage-for-android-revisited-44789c9b722f)



##### 2. 其他资料

* 演示项目地址

  [AndroidBDDExample](https://github.com/zeshaoaaa/AndroidBDDExample)



