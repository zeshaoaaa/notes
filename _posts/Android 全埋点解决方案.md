## 1. 全埋点概述

全埋点也叫无埋点、无码埋点、无痕埋点、自动埋点。全埋点指的是不需要 Android 开发者写代码，或写很少代码，就能预先收集用户的所有行为数据，然后可以根据实际的业务分析需求，从中筛选出所需行为数据并进行分析。

全埋点采集的事件主要包括以下 4 种，事件前面的 $ 符号表示该事件是预置事件，与之对应的是自定义事件。

* $AppStart 事件

  应用启动事件，包括冷启动和热启动，热启动就是应用从后台恢复的情况。

* $AppEnd 事件

  应用退出事件，包括正常退出、进入后台、被强杀、崩溃等场景。

* $AppViewScreen 事件

  页面浏览事件，就是切换 Activity 或 Fragment。

* $AppClick 事件

  控件点击事件，比如 Button 被点击。

在这四种事件中，最重要而且采集难度最大的，就是 $AppClick 事件，所以全埋点解决方案也是围绕如何采集 $AppClick 事件进行的。

对于 $AppClick 事件的全埋点整体解决思路，归根结底就是自动找到那个被点击的控件处理逻辑（原处理逻辑）。

利用一定的技术原理，对原处理逻辑进行“拦截”，或在原处理逻辑的前后插入对应的埋点代码，从而达到自动埋点的效果。

### 1.1 Android 事件分发机制

要自动拦截控件的原处理逻辑，就要了解 Android 的事件分发机制。

事件分发的对象是点击事件（Touch 事件），该事件在用户触摸屏幕（View、ViewGroup）时会产生。

从手指接触屏幕到离开屏幕的过程中，会产生一系列事件。

![移动手指产生的事件.png](https://upload-images.jianshu.io/upload_images/2004563-6074046adb64f444.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

事件分发过程，由 dispatchTouchEvent() 、onInterceptTouchEvent() 和 onTouchEvent() 三个方法协作完成。





### 1.1 Android View 类型 

























































### 1.2 View 绑定 listener 方式

## 2. $AppViewScreen 全埋点方案

## 3. $AppStart、$AppEnd 全埋点方案































## 参考资料

[《Android 全埋点解决方案》](https://book.douban.com/subject/33400077/)

[《Android事件分发机制》](https://blog.csdn.net/carson_ho/article/details/54136311)

