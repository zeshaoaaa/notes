Stream 、Bloc 与响应式编程介绍，包含了理论与案例，难度：中级；

### 前言

我花了不少时间，才想明白要怎么解释响应式编程、Bloc 与 Stream 这几个概念，由于这些概念能让一个应用的架构发生很大的变化，所以我想用一个实战案例来说明。

下面这个应用的功能：

* 从在线目录查看电影列表
* 通过种类和上映日期筛选想要的电影
* 收藏/取消收藏电影

这些功能都是交互式的，用户动作可以发生在不同的页面，或者是在同一个页面并且有影响在可见方面，实时。

当用户登录后



## 1. 什么是 Stream？

### 1.1 Stream 简介

![管道](/Users/jay/Pictures/文章插图/Flutter/管道.png)

我们可以把 Flutter 的 Stream 看做是一条管道，控制 Stream 的叫 StreamController，我们要插入数据到 Stream 中时，StreamController 就会暴露入口（StreamSink），通过 sink 属性可以访问这个入口，管道输出的方法由 StreamController 的 stream 属性暴露。

不论是事件、对象、集合、错误甚至是另一个 Stream，任何类型的数据都能用 Stream 传递。



#### 1.1.1 怎么知道有东西通过 Stream 传过来？

如果我们想在某个 Stream 传东西出来时收到通知，只需要监听 StreamController 的 Stream 属性，当我们定义了一个监听器（listener）后，就能拿到一个 StreamSubscription 对象，通过这个对象我们就能收到对应的通知。





## 参考资料

* [Reactive Programming — Streams — BLoC](https://www.didierboelens.com/2018/08/reactive-programming-streams-bloc/)

