### 3.6 认识视图（Views）

#### 3.6.1 Widget 简介

* Android

  在 Android 中，View 是屏幕上显示的所有内容的基础，比如按钮、工具栏、输入框等都是 View；

* iOS

  在 iOS 中，构建 UI 的过程中将大量使用 view 对象，这些对象都是 UIView 的实例，它们可以用作容器来承载其他的 UIView，最终构成你的界面布局；

* React Native

  在 RN 中，View 是一个支持 Flexbox 布局的容器、样式、触摸处理和辅助控制；

Widget 与 View 的区别：

* 生命周期

  我们可以把 Wdiget 看做是“声明和构建 UI 的方法”，Widget 具有不同的生命周期，是不可变的，会存在与状态被改变之前。

  每当 Widget 或其状态发生变化时，Flutter 的框架都会创建一个新的 Widget 实例树，而 Android 或 iOS 的视图被绘制后，只有在调用 invalidate/setNeedsDisplay 后才会重新绘制。

* 轻巧

  Flutter 的 Widget 很轻巧，因为它本身不是视图，不会直接绘制任何东西，而是对 UI 及其语义的描述



在 Android 方面，Flutter 包含了 Material 组件库，这些组件遵循了 Material 设计规范，在 iOS 平台，我们可以用 Cupertino Widgets 构建遵循 iOS 设计语言的界面。



#### 3.6.2 更新 Widget

在 Andorid/iOS 中要更新视图时，我们可以直接通过对应的方法来操作更改，但是在 Flutter 中，Widget 是不可变的，我们可以通过操控 Widget 的状态更新 Wdiget。

Flutter 中有 StatelessWidget（无状态组件）和 StatefulWidget（有状态组件），当我们描述的用户界面不依赖于对象中的配置信息时，我们可以使用 StatelessWidget。

比如我们想显示 Logo，而 Logo 是在运行时不会改变的，所以就可以用 StatelessWidget，如果是要根据网络请求或用户交互后更改 UI ，就可以使用 StatefulWdiget。

它们之间最重要的区别，就是 StatefulWidget 有一个 State 对象，该对象负责存储状态数据，并将数据传递到树重建中，所以状态不会丢失。



#### 3.6.3 布局

* Android

  使用 XML 编写布局；

* iOS

  1. 用 Storyboard 文件组织 Views，并设置它们的约束；
  2. 在 ViewController 中用代码设置约束；

* Flutter

  编写 Widget 树声明布局；

```dart
@override
Widget build(BuildContext context) {
  return Scaffold(
  	appBar: AppBar(
    	title: Text("示例应用")
    )
  )
}
```



#### 3.6.4 动态添加/删除 Widget

* Android

  调用 ViewGroup 的 addChild/removeChild 方法；

* iOS

  调用父 View 的 addSubview 或调用子 View 的 removeFromSuperview；

* Flutter 

  传入一个函数或表达式，该函数或表达式返回一个 Widget 给父项，并通过布尔值控制该 Wdiget 的创建；



#### 3.6.5 动画

* Android

  1. 通过 XML 创建动画
  2. 调用 view.animate() 或通过 ValueAnimator/ObjectAnimator；

* iOS

  调用 animte() 方法给 View 创建动画

* Flutter

  用动画库包裹 Widget；

AnimationController

 是一个可以暂停、寻找、停止、反转动画的 Animation 类型，它需要一个 Ticker ，当 vsync 发生时发送信号，并且在每帧运行时，创建一个介于 0 和 1 之间的线性插值（Interpolation），可以创建一个或多个 Animation ，并附加给一个 Controller。

比如我们可以用 CurvedAnimation 实现一个 interpolated 曲线，在这个场景中，Controller 是动画过程的主人，而 CurvedAnimation 负责计算曲线，并替代 Controller 默认的线性模式。

在构建 Widget 树时，我们要把 Animation 指定给一个 Widget 的动画属性，比如 FadeTransition 的 opacity（透明度），并告诉控制器开始动画。

