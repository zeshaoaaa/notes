## 5. 代码实验室

### 5.4 Flutter 布局

欢迎来到 Flutter 布局代码实验室，你可以在这里学到怎么构建一个 Flutter UI ，不用下载和安装 Flutter 或 Dart。

代码实验室涵盖基础的 Flutter 布局概念，用的是实验代码编辑器 DartPad，DartPad 没有测试过在所有浏览器上的使用情况。

如果你在特定的浏览器使用 DartPad 时遇到了任何困难，请创建一个 DartPad issue 并且在 issue 的标题中说明你用的是哪个浏览器。

Flutter 和其他的框架不同，因为它的 UI 是用代码构建的，而不是像 Android 一样在 XML 中。

组件是 Flutter 应用的基础构建块，接下来讲得最多的就是 Flutter 组件。

一个组件是一个不可变的对象，描述了用户界面的特定部分，组件是可组合的，我们可以把组件合成一个更复杂的组件。

学完这一节你就知道怎么用 Flutter 显示一张名片。

#### 5.4.1 Row 与 Column

Row 与 Column 是包含并摆开其他组件的类，Row 或 Column 中的组件叫作子组件（Children），Row 和 Column 就叫作在这些组件的父组件（Parent）。

Row 会把组件水平地摆开，Column 则会垂直地把组件展开。

```dart
import 'dart:async';
import ':package:flutter/material.dart';
import ':package:flutter_test/flutter_test.dart';

class MyWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Row(
    	children: [
        BlueBox(),
        BlueBox(),
        BlueBox()
      ]
    )
  }
  
}

class BlueBox  extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
    	width: 50,
    	height: 50,
      decoration: BoxDecoration(
      	color: Colors.blue,
        border: Border.all(),
      ),
    );
  }
}
```



#### 5.4.2 轴大小与对齐

上面的 BlueBox 组件是连在一起的，我们可以用轴大小和对齐属性把它们隔开。



##### 1. 主轴线尺寸属性

Row 与 Column 主轴线是不同的，Row 的主轴线是水平的，Column 的主轴线是垂直的，mainAxisSize 属性决定了多少控件一个 Row 或 Column 可以持有在它们的主轴线。

mainAxisSize 属性有两个值：

1. MainAxisSize.max

   mainAxisSize 的默认值，max 表示 Row 和 Column 会持有主轴线上的全部空间，子组件会向左对齐。

2. MainAxisSize.min

   min 表示 Row 与 Column 只持有主轴线上足够子组件使用的的空间，子组件会摆在主轴线的中间。




##### 2. 主轴线对齐属性

当 mainAxisSize 设为 max 时，Row 和 Column 会用额外的空间放置子组件，mainAxisAlignment 属性决定 Row 与 Column 可以放置它们的子组件在额外空间。







## 参考资料

[Flutter 官方文档](https://flutter.dev/docs/codelabs)