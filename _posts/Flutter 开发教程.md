## 1. 组件简介

Flutter 组件是受到了 React 启发来构建的，其主要思想就是我们可以在组件外构建 UI，组件描述了它们当前的配置和状态下应该展示的视图。

当一个组件的状态改变时，组件会重新构建它的描述，Flutter 会对比组件之前的描述和当前描述，以决定渲染树转移从一个状态到下一个状态需要的最小变化。

如果你想通过钻研代码熟悉 Flutter，你可以额看到[布局实验室](https://flutter.dev/docs/codelabs/layout-basics)、[布局构建方法](https://flutter.dev/docs/development/ui/layout)以及[添加交互性方法](https://flutter.dev/docs/development/ui/interactive)。



### 1.1 Hello World

下面是一个最简单的 Flutteer 应用，只传了一个组件给 runApp() 函数。

```dart
import 'packagee:flutteer/material.dar';

void main() {
  runApp(
  	Center(
    	child: Text(
      	'Hello ,world!',
        textDirection: TextDirection.ltr,
      ),
    ),
  );
}
```

runApp() 函数接收到一个组件后会该组件作为组件树的根组件，在上面这段代码中，组件树由两个组件组成，Center 组件和它的子组件 Text 组件。

Fluttere 框架会强制根组件铺满屏幕，也就是 “Hello，world” 会被放在屏幕中间，上面这段代码还指定了 textDirection 属性，ltr 表示从左到右（left to right）。

在开发 App 时，我们要根据组件是否需要管理状态来判断组件是继承 StatelessWidgete 还是 StatefulWidget，组件主要的工作就是实现 build() 函数，这个函数描述组件在其他方面、低级组件。

Fluttere 框架会逐一构建组件，直到到 RenderObject 为止，RenderObject 会计算并描述组件的几何图形。





## 6. 添加 Flutter 模块到原生应用

### 6.1 简介

#### 6.1.1 添加到应用

一次性把原生应用转换成 Flutter 是不现实的，我们可以逐步地把新开发的 Flutter 库或 Flutter 模块添加到已有的 iOS 或 Andorid 项目中，把一部分的页面用 Flutter 实现，又或者是共用 Dart 实现的业务逻辑。

接下来我们就来看下怎么把 Flutter 的高效和强表现力加入到应用中。

从 Flutter 1.12 开始，Flutter 就支持添加 Flutter 代码到原生项目中，但是有如下限制：

- 运行多个 Flutter 实例或运行在部分屏幕的控件可能有未定义的行为；

- 没办法在背景模式下使用 Flutter（该功能正在开发中）；

- 一个原生项目只允许有一个 Flutter 库；

- 在 Android 的用在“添加到应用

- ”的插件，应该基于 [FlutterPlugin](https://api.flutter.dev/javadoc/io/flutter/embedding/engine/plugins/FlutterPlugin.html) 迁移到新的 Android 插件。

  不支持 FlutterPlugin 的插件可能有意料之外的行为，如果它们假设在“添加到应用”是站不住脚的，比如假设 Flutter Activity 总是存在。

- 从 1.17 开始，Flutter 模块（Module）只支持 AndroidX 应用；

#### 6.1.2 支持的特性

##### 1. 添加到 Android 应用中

- 通过添加 Flutter SDK hook 到 Gradle 脚本中，就可以实现自动构建并导入 Flutter 模块。

- 把 Flutter 模块打包成一个 AAR（Android Archive）；

- FlutterEngine API 可以独立地启动并保持 Flutter 环境，而不是附着于一个 FlutterActivity 或 FlutterFragment 等。

- Android Studio 支持编辑 Flutter 和 Android 代码以及创建模块和导入指引，而且不论是 Java 还是 Kotlin 的项目都可以使用。

- Flutter 模块可以使用 Flutter 插件以交互与平台。Android 插件应该迁移到 V2 插件 API 为了更好的“添加到应用”正确性。

  从 Flutter 1.12 开始，大多数的插件都由 Flutter 团队维护，FlutterFire 也已经迁移了。

- 支持 Flutter 调试与有状态的热加载通过使用 flutter attach 从 IDEA 或命令行到连接到一个应用包含 Flutter。



## 6.2 集成 Flutter 模块到 Android 项目

Flutter 可以嵌入到我们已有的 Android 应用中，作为源码 Gradle 子项目或作为 AAR。

我们可以用装了 Flutter 插件的 Android Studio 来集成 Flutter 模块，也可以手动集成。

#### 6.2.1. CPU 架构筛选

你现有的 Android 应用可能支持 mips 或 x86 架构，但是 Flutter 现在只支持构建 AOT（Ahead-of-time）编译的库（x86_64、armeabi-v7a 以及 arm64-v8a）。

我们可以在 gradle 配置中用 abiFilters 限制支持的架构，避免没有 libflutter.so 时程序崩溃，比如下面这样的配置：

```
android {
  // ...
  defaultConfig {
    ndk {
      // 支持 Flutter 的架构的过滤器
      abiFilters 'armeabi-v7a', 'arm64-v8a', 'x86_64'
    }
  }
}
```



#### 6.2.2 使用 Android Studio 集成 Flutter 模块

Android Studio 可以很方便地自动集成 Flutter 模块，使用 Android Studio，你可以在同一个项目中编辑 Android 代码和 Flutter 代码。

你还可以继续使用你的正常的 IntelliJ Flutter 插件功能比如 Dart 代码补全、热加载以及组件检查器。

“添加到应用”流与 Android Studio 只支持装了 Flutter 插件（版本 42+）的 Andorid Studio 3.6 。

用 Android Studio 创建并集成 Flutter 模块的话就不能用 AAR 的方式集成，而是要用源码 Gradle 子项目的方式集成。



#### 6.2.3 手动集成 Flutter 模块

##### 1. 创建 Flutter 模块

##### 2. 声明编译选项

##### 3. 添加 Flutter 模块作为依赖

接下来有两种方式可以添加 Flutter 模块作为我们已有应用的依赖，AAR 的方式创建通用的 Android AAR 作为Flutter 模块中间的包。

当你的下流应用构建起不想要有 Flutter SDK 安装时就可以用这种方式。

这种方式添加一个或更多构建步骤如果你频繁构建。



- 选项 A：依赖 AAR

  

- 选项 B：依赖模块源码

  使用依赖 Flutter 模块源码的方式集成 Flutter 模块，可以一起构建 Android 项目和 Flutter 项目。

  如果你工作在部分同时地并且快速的迭代，不过你的团队必须安装 Flutter SDK 以构建这个主应用，这时就可以用这种方式。

