## 1. Jetpack 简介

Jetpack 是一套可以帮助我们更轻松地写出高质量的应用的库、工具和指南。

Jetpack 包含一系列组件，有了这些组件我们就不用再写大量的样板代码，而且这些组件可以帮助我们简化复杂的任务，这样我们就可以集中注意力在我们关心的代码上。

Jetpack 由 androidx.* 库组成，从平台 API 中独立出来，也就是它提供了向后兼容，并且它更新比 Android 平台更频繁，确保我们总是访问最新的 Jetpack 组件版本。



### 1.1 Jetpack 的3个特点

1. 加速开发

   Jetpack 组件不是捆绑在一起的，我们可以独立地使用 Jetpack 的各个组件，不过一起使用这些组件并且使用 Kotlin 进行开发能让我们更加高效。

2. 减少样板代码

3. 构建高质量且健壮的应用



### 1.2 Jetpack 的 4 种组件

```mermaid
classDiagram
      Animal <|-- Duck
      Animal <|-- Fish
      Animal <|-- Zebra
      Animal : +int age
      Animal : +String gender
      Animal: +isMammal()
      Animal: +mate()
      class Duck{
          +String beakColor
          +swim()
          +quack()
      }
      class Fish{
          -int sizeInFeet
          -canEat()
      }
      class Zebra{
          +bool is_wild
          +run()
      }
```



mysql -h aa7toldr9mlkg5.cl3ywuabnthp.us-west-2.rds.amazonaws.com -P 3306 -u DBUser -p 12345678

```java
public void foo() {
  
}
```









