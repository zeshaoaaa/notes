# Android 面试问题

## 项目经验

### 锁屏杂志

- Bug

	- 做这个 App 的过程中遇到了哪些 Bug ？是怎么解决的？最难的是哪一个？

- 优化

	- 内存优化

		- 管理机制

			- Java 的内存管理机制是怎么样的？
			- Java 的内存回收算法是怎么样的？
			- Android 的内存管理机制是怎么样的？

		- 什么是内存泄漏？
		- 什么是内存抖动？
		- 内存优化的工具有哪些？

			- LeakCanary

				- LeakCanary 的工作原理是怎么样的？

		- 遇到哪些内存问题？
		- 监控

			- 线上内存监控手段有哪些？

	- APK 瘦身

		- APK 瘦身的方法有哪些？
		- 怎么对代码进行瘦身？
		- 怎么优化 lib 库体积？

	- 布局优化

		- 布局优化是怎么做的？

- 兼容

	- 遇到了哪些兼容问题？

- 暗色模式

	- 怎么实现的？

- 框架

	- Retrofit

		- Retrofit 的原理是什么？
		- 代理的方式有哪两种？

			- 什么是静态代理？
			- 什么是动态代理？

	- RxJava2

		- RxJava 的原理是什么？
		- RxJava 有哪些运算符？

	- OkHttp

		- OkHttp 工作机制是怎么样的？
		- OKHttp有多少个拦截器？每个拦截器的工作机制是怎么样的？

	- MVVM

		- 怎么实现 MVVM？
		- MVVM 的优势在哪里？

	- Kotlin

		- Kotlin 的优势在哪里？
		- Kotlin 的扩展函数怎么使用？
		- Kotlin 调用 Java 代码时有哪些坑？
		- 协程

			- 什么是协程？
			- 协程跟线程有什么区别？
			- 怎么使用协程？

	- JetPack

		- ViewModel 的工作原理是怎么样的？
		- Lifecycle 的工作原理是怎么样的？
		- LiveData 的工作原理是怎么样的？
		- Room

			- Room 和 SQLite 的区别在哪里？
			- Room 的工作原理是怎么样的？

- 埋点

	- 怎么实现的埋点？
	- AOP

		- AspectJ

			- AspectJ 的原理是什么？
			- 怎么使用 AspectJ 进行埋点？

	- ASM

		- 什么是 ASM？
		- 怎么使用 ASM ？

- ProtoBuf

	- 为什么要使用过 ProtoBuf ？
	- 怎么使用 ProtoBuf？

